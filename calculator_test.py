"""
Calculator web tests
"""

# pylint: disable=W0621,C0305

import re
import pytest
from calculator import create_app

@pytest.fixture()
def app():
    """App"""
    new_app = create_app()
    new_app.config.update({
        "TESTING": True,
    })
    yield new_app

@pytest.fixture()
def client(app):
    """Client"""
    return app.test_client()

def test_addition_invalid_v1(client):
    """Should error when v1 is not provided"""
    response = client.get("/addition?v1=a&v2=3")
    assert 400 == response.status_code
    assert b"ERROR: Invalid input(s)" in response.data

def test_addition_invalid_v2(client):
    """Should error when v2 is not provided"""
    response = client.get("/addition?v1=2&v2=a")
    assert 400 == response.status_code
    assert b"ERROR: Invalid input(s)" in response.data

def test_addition_calculation(client):
    """Should correcly calculate the result of v1+v2"""
    response = client.get("/addition?v1=2&v2=3")    
    assert 200 == response.status_code
    regex = "<input type=\"number\" id=\"result\" name=\"result\" value=\"(\\d+)\" disabled>"
    result_search = re.search(regex, response.data.decode("ASCII"))
    result = result_search.group(1)
    # 2 + 3 = 5
    assert "5" == result
