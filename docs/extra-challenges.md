# Extra challenges
In this section we will introduce a couple of objectives that will improve your pipelines and approximate them to what you would find in a real project.

## Enforce code quality
1. Prevent pushing to main to enforce merge requests and code reviewing.
> **TIP:** Look into the repository settings
## Protect the production environment
2. Change the **.gitlab-ci.yaml** to only run the deployment job for the main branch.
3. Change the **.gitlab-ci.yaml** to run only one deployment job at a time.
> **TIP:** Look into the Gitlab CI/CD documentation
## Improving the CI/CD pipeline
4. The install of dependencies is forced to run before **each** job because the container running it is destroyed after every job. You can change the image used to one that already has all necessary dependencies.
> **TIP:** Dockerhub should have what you need
5. Tests, build and deploy jobs run regardless of what file has changed in the repository. Change the **.gitlab-ci.yaml** to run each job according to the files changed.
> **TIP:** Look into the Gitlab CI/CD documentation